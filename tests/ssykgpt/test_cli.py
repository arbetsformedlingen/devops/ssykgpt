import unittest
import subprocess
from subprocess import Popen
import json

def load_text(filename):
    with open(filename, "r") as f:
        return f.read()
    
def test_basic_cli_use():
    ad_text = load_text("tests/testdata/input.1")
    with Popen(['poetry',
                'run',
                'python3',
                'src/ssykgpt/cli.py'],
               stdout=subprocess.PIPE, stdin=subprocess.PIPE) as proc:
        proc.stdin.write(ad_text.encode('utf-8'))
        proc.stdin.close()

        output = []
        for line in proc.stdout:
            output.append(json.loads(line.decode('utf-8')))

        assert 1 == len(output)
        assert "2221" == output[0]["ssykgpt"]
