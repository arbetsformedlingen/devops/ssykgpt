# vim:fenc=utf-8
from click.testing import CliRunner
import ssykgpt.core as ssykgpt
import json
from dotenv import load_dotenv
import time

load_dotenv()

def read_json(filename):
    with open(filename, "r") as f:
        return json.load(f)

def test_ad_processor_had():
    ad = read_json("tests/testdata/input.1")
    processor = ssykgpt.AdProcessor({"inputformat": "headline-and-description"})
    result = processor.process_ad(ad)
    assert result["ssykgpt"] == '2221'

def test_ad_processor_joblinks():
    ad0 = read_json("tests/testdata/input.1")
    headline = ad0["headline"]
    description = ad0["description"]["text"]
    ad = {"originalJobPosting": {"title": headline, "description": description}}
    processor = ssykgpt.AdProcessor({"inputformat": "joblinks"})
    result = processor.process_ad(ad)
    assert result["ssykgpt"] == '2221'

def test_rate_limiter():
    def timedif_near(a, b, gap):
        return abs((b - a) - gap) < 0.1
    
    rl = ssykgpt.RateLimiter(0.75)
    a = time.time()
    rl.sleep() # 0
    b = time.time()
    rl.sleep() # 0.75
    c = time.time()
    time.sleep(0.25)
    d = time.time()
    rl.sleep() # 0.5
    e = time.time()

    assert timedif_near(a, b, 0.0)
    assert timedif_near(b, c, 0.75)
    assert timedif_near(c, d, 0.25)
    assert timedif_near(d, e, 0.5)
