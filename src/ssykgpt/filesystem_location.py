import pathlib

def nth_parent(p, n):
    result = p
    for i in range(n):
        result = result.parent
    return result

def is_project_root(p):
    return p.joinpath("src", "ssykgpt", "filesystem_location.py").exists() and p.joinpath("Makefile").exists() and p.joinpath("pyproject.toml").exists()

def resolve_root_path_from_candidates(root_candidates):
    valid_roots = [p for p in root_candidates if is_project_root(p)]
    n = len(valid_roots)
    if n == 1:
        return valid_roots[0]
    elif n == 0:
        raise RuntimeError("No valid root path found among {:s}".format(root_candidates))
    else:
        raise RuntimeError("Ambiguous root path ({:s})".format(str(valid_roots)))

def resolve_root_path():
    this_file = pathlib.Path(__file__)
    return resolve_root_path_from_candidates([
        nth_parent(this_file, 3), # <-- in case we run this from source
        nth_parent(this_file, 6).joinpath("src/ssykgpt") # <-- in case we run it as an installed pip library
    ])
