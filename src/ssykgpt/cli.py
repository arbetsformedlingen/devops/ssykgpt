import logging
import click
from dotenv import load_dotenv
from os.path import isfile
import os
import ndjson
import traceback
from ssykgpt.core import AdProcessor
import sys

def try_read_sysprompt_file(syspromptfile):
    if syspromptfile and isfile(syspromptfile) and os.access(syspromptfile, os.R_OK):
        with open(syspromptfile, "r") as infile:
            return infile.read()

def read_ads_stream(file):
    reader = None
    if file and isfile(file) and os.access(file, os.R_OK):
        infile = open(file, "r")
        reader = ndjson.reader(infile)
    else:
        reader = ndjson.reader(sys.stdin)
    for ad in reader:
        yield ad

def print_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

@click.option("--sysprompt", "-s", type=str, required=False, help="The system prompt.")
@click.option("--syspromptfile", "-S", type=str, required=False, help="The system prompt as a file. Overrides --sysprompt")
@click.option("--joblinks", "-J", is_flag=True, help="The joblinks input format.")
@click.option("--bigsearchfile", "-b", type=str, required=False, help="When searching for data in a set larger than modelmax, use this.")
@click.option("--bigsearchdatafilterrx", "-Q", type=str, required=False, help="A regex capturing what's important.")
@click.option("--queryprompt", "-q", type=str, required=False, help="Use to prefix prompts.")
@click.option("--openaimodel", "-m", type=str, required=False, help="Specify Chat Completion OpenAI model.")
@click.option("--modelmaxresponse", "-r", type=int, required=False, help="The chosen models max context.")
@click.option("--openaitemp", "-t", type=float, required=False, help="Model temperature.")
@click.option("--api_key", "-k", type=str, required=False, help="API key.")
@click.option("--desiredresponselength", "-l", type=int, required=False, help="The desired length of the response.")
@click.option("--adfile", type=str, required=False, help="JSONL file with ads, overrides stdin.")
@click.option('--log', is_flag=True, help='Enable logging')
@click.command(context_settings=dict(help_option_names=["-h", "--help"])) # alias -h to --help
def cli(sysprompt,
        syspromptfile,
        joblinks,
        bigsearchfile,
        bigsearchdatafilterrx,
        queryprompt,
        openaimodel,
        modelmaxresponse,
        openaitemp,
        api_key,
        desiredresponselength,
        adfile,
        log) -> None:
    load_dotenv()
    settings = {
        'sysprompt': sysprompt or try_read_sysprompt_file(syspromptfile),
        'queryprompt': queryprompt,
        'bigsearchfile': bigsearchfile,
        'bigsearchdatafilterrx': bigsearchdatafilterrx,
        'openaimodel': openaimodel,
        'modelmaxresponse': modelmaxresponse,
        'openaitemp': openaitemp,
        'api_key': api_key,
        'desiredresponselength': desiredresponselength,
        'syspromptfile': syspromptfile,
        'inputformat': 'joblinks' if joblinks else 'headline-and-description'
    }
    if log:
        settings['loglevel'] = logging.DEBUG
    try:
        processor = AdProcessor(settings)
        for ad in read_ads_stream(adfile):
            try:
                result = processor.process_ad(ad)
                output_json = ndjson.dumps([result], ensure_ascii=False)
                print(output_json, file=sys.stdout)
            except:
                print(traceback.print_exc())
                e = sys.exc_info()[0]
                print_error(e)
                exit(1)
    except Exception:
        print(traceback.print_exc())
        print(traceback.format_exc())

if __name__ == "__main__":
    cli()
