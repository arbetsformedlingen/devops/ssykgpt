import os
import re
import time
import sys
from openaipy import functions
import json
import ndjson
from ssykgpt.filesystem_location import resolve_root_path

default_settings = {
    'sysprompt': "Du är SSYKGPT, en språkmodell som analyserar en jobbannons och tilldelar den en yrkeskod i SSYK-standard. Ditt svar skall endast bestå av en fyrställig SSYK-kod. Välj mellan dessa SSYK-koder:",
    'queryprompt': "Här är är en jobbannons du ska analysera: ",
    'bigsearchfile': "data/code+labels.txt",
    'bigsearchdatafilterrx': r"^.*(\d\d\d\d).*$",
    'openaimodel': "gpt-3.5-turbo",
    'modelmaxresponse': 4096,  # this is documented for each model on OpenAI
    'openaitemp': 0.1,
    'desiredresponselength': 100,
    'inputformat': 'text-and-description',
    'delay': 50
}

def _remove_none_values(m):
    return {k:v
            for (k, v) in m.items()
            if v is not None}

def _text_from_joblinks(ad):
    text = ""
    if 'originalJobPosting' in ad:
        if 'title' in ad['originalJobPosting']:
            text += ad['originalJobPosting']['title']
            text += "\n\n\n"
        if 'description' in ad['originalJobPosting']:
            text += ad['originalJobPosting']['description']
    return text

def _text_from_headline_and_description(ad):
    text = ""
    if 'headline' in ad:
        text += ad['headline']
        text += "\n\n\n"
    if 'description' in ad:
        text += ad['description']['text']
    return text


text_from_input_format_map = {
    "joblinks": _text_from_joblinks,
    "headline-and-description": _text_from_headline_and_description
}

def _complete_settings(specified_settings):
    api_key_setting = {'api_key': os.getenv('OPENAI_API_KEY')}
    settings = default_settings | _remove_none_values(api_key_setting) | _remove_none_values(specified_settings)
    api_key = settings.get('api_key')
    if api_key is None:
        raise RuntimeError("Missing required setting `api_key`: Either the environment variable `OPENAI_API_KEY` or the setting `api_key` must be defined.")
    settings = settings | functions.init(api_key)
    loglevel = settings.get("loglevel")
    if loglevel is not None:
        del settings["loglevel"]
        settings['logger'].setLevel(level=loglevel)
    assert(settings["inputformat"] in text_from_input_format_map)
    return settings

def _load_code2label(filename):
        with open(filename) as taxotxt:
            taxo = json.load(taxotxt)
        return {node['ssyk_code_2012']:node['preferred_label']
                for node in taxo['data']['concepts']}

class RateLimiter:
    def __init__(self, delay):
        self._earliest = 0.0
        self._delay = delay

    def sleep(self):
        duration_to_sleep = self._earliest - time.time()
        if 0 < duration_to_sleep:
            time.sleep(duration_to_sleep)
        self._earliest = time.time() + self._delay
    
class AdProcessor:
    def __init__(self, user_settings):
        self._project_root = resolve_root_path()
        self._settings = _complete_settings(user_settings)
        self._code2label = _load_code2label(self._project_root.joinpath('data', 'nivå-fyra-i-ssyk-strukturen.json'))
        self._text_from_ad = text_from_input_format_map[self._settings["inputformat"]]
        self._rate_limiter = RateLimiter(self._settings["delay"])
        
    def process_ad(self, ad):
        settings = self._settings
        self._rate_limiter.sleep()
        text = self._text_from_ad(ad)
        if not text:
            settings['logging'].error("Add has no text: " + str(ad))
            raise RuntimeError()

        bigsearchfile = settings.get("bigsearchfile")
        bigsearchdatafilterrx = settings.get('bigsearchdatafilterrx')
        if bigsearchfile is not None and bigsearchdatafilterrx is not None:
            response, message_log, settings = functions.bigsearch(
                settings, [], str(self._project_root.joinpath(bigsearchfile)),
                bigsearchdatafilterrx, text)
        else:
            response, message_log, settings = functions.converse(settings, [], text)
        response = re.sub(bigsearchdatafilterrx, r"\1", response) # extract four digit SSYK code in case there is junk too
        return {**ad,
                'ssykgpt': response,
                'ssykgpt_taxonomy_label': self._code2label.get(response, "n/a")}
