#!/bin/bash
set -euo pipefail

. settings.sh

mkdir -p "$OUT"

for START in $(seq 1 1 4); do
    STOP=$START
    echo `date`"] $START - $STOP" >&2
    time bash run_batch.sh "$OUT"/result.start_"$START".stop_"$STOP" "$START" "$STOP" \
	 -l 100 -t 0.1 --bigsearchfile data/code+labels+desc.txt2 --log
    exit
done
exit
for START in $(seq "$SEQ_START" "$SEQ_INC" "$SEQ_STOP"); do
    STOP=$(( START + 9 ))
    echo `date`"] $START - $STOP" >&2
    time bash run_batch.sh "$OUT"/result.start_"$START".stop_"$STOP" "$START" "$STOP" \
	 -l 100 -t 0.1 --bigsearchfile data/code+labels+desc.txt2 --log
    sleep 60
done
