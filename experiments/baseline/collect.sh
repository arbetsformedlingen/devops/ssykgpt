#!/bin/bash
set -euo pipefail

. settings.sh

for START in $(seq "$SEQ_START" "$SEQ_INC" "$SEQ_STOP"); do
    STOP=$(( START + 9 ))
    zcat "$OUT"/result.start_"$START".stop_"$STOP".2.gz | jq -c .
done
