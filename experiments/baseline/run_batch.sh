#!/bin/bash
set -euo pipefail

#INPUT=~/2022q2_beta1_jsonl/2022.enriched.jsonl
INPUT=~/2022_beta1_jsonl/2022.enriched.jsonl.gz
OUTPREF="$1" ; shift
RANGE_FROM="$1" ; shift  # 1 = first line
RANGE_TO="$1" ; shift
SSYKARGS=$@

test -f "$OUTPREF".0 && { echo "**** remove output files first" >&2; exit 1; }

set -x
zcat "$INPUT" \
| sed -n "$RANGE_FROM,$RANGE_TO p" \
| poetry run ssykgpt $SSYKARGS > "$OUTPREF".0 2>"$OUTPREF".0.err

cat "$OUTPREF".0 \
    | jq -c '. + { originalJobPosting: { description: .description.text } }' \
    | podman run --rm -i docker-images.jobtechdev.se/joblinks/text-2-ssyk:latest \
    | jq -c 'del(.originalJobPosting)' >"$OUTPREF".1 2>"$OUTPREF".1.err

cat "$OUTPREF".1 \
    | >"$OUTPREF".2 2>"$OUTPREF".2.err jq -c --slurpfile ssykmap data/nivå-fyra-i-ssyk-strukturen.json -f <(cat <<"EOF"
(INDEX($ssykmap[] | .data.concepts[]; .ssyk_code_2012) | map_values(.preferred_label)) as $dict
| { headline: .headline,
    description: .description.text,
    ssykgpt: .ssykgpt,
    ssykgpt_taxonomy_label: .ssykgpt_taxonomy_label,
    ssyk_simon: .ssyk_lvl4|tostring,
    ssyk_simon_label: $dict[.ssyk_lvl4|tostring],
    legacy_ams_taxonomy_id: [ .occupation_group[] | .legacy_ams_taxonomy_id ] | join(", "),
    legacy_ams_taxonomy_label: [ .occupation_group[] | .label ] | join(", "),
    enriched: ( .keywords.enriched // "" | if . == "" then "n/a" else .occupation | join(", ") end )
  }
EOF
)

gzip "$OUTPREF".*
