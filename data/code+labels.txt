SSYK 3511: Drifttekniker, IT. 
SSYK 3449: Övriga utbildare och instruktörer. 
SSYK 9210: Bärplockare och plantörer m.fl.. 
SSYK 8172: Processoperatörer, papper. 
SSYK 7522: Fin-, inrednings- och möbelsnickare. 
SSYK 5111: Kabinpersonal m.fl.. 
SSYK 2235: Röntgensjuksköterskor. 
SSYK 1420: Chefer inom förskoleverksamhet. 
SSYK 9413: Kafé- och konditoribiträden. 
SSYK 2671: Präster. 
SSYK 4117: Medicinska sekreterare, vårdadministratörer m.fl.. 
SSYK 2320: Lärare i yrkesämnen. 
SSYK 6112: Trädgårdsodlare. 
SSYK 3111: Ingenjörer och tekniker inom industri, logistik och produktionsplanering. 
SSYK 2651: Bildkonstnärer m.fl.. 
SSYK 3214: Tandtekniker och ortopedingenjörer m.fl.. 
SSYK 2351: Speciallärare och specialpedagoger m.fl.. 
SSYK 5326: Ambulanssjukvårdare. 
SSYK 3431: Fotografer. 
SSYK 2135: Specialister och rådgivare inom skogsbruk. 
SSYK 8332: Lastbilsförare m.fl.. 
SSYK 7126: Kyl- och värmepumpstekniker m.fl.. 
SSYK 8342: Anläggningsmaskinförare m.fl.. 
SSYK 7121: Takmontörer. 
SSYK 1120: Verkställande direktörer m.fl.. 
SSYK 3116: Ingenjörer och tekniker inom gruvteknik och metallurgi. 
SSYK 5227: Apotekstekniker. 
SSYK 2161: Arkitekter m.fl.. 
SSYK 3332: Evenemangs- och reseproducenter m.fl.. 
SSYK 3522: Ljus-, ljud- och scentekniker. 
SSYK 8151: Maskinoperatörer, blekning, färgning och tvättning. 
SSYK 3250: Tandhygienister. 
SSYK 1350: Fastighets- och förvaltningschefer. 
SSYK 2330: Gymnasielärare. 
SSYK 3352: Skattehandläggare. 
SSYK 4223: Telefonister. 
SSYK 7224: Slipare m.fl.. 
SSYK 8171: Processoperatörer, pappersmassa. 
SSYK 5350: Tandsköterskor. 
SSYK 2343: Förskollärare. 
SSYK 7612: Bagare och konditorer. 
SSYK 2663: Biståndsbedömare m.fl.. 
SSYK 2234: Företagssköterskor. 
SSYK 2613: Åklagare. 
SSYK 2227: Geriatriksjuksköterskor. 
SSYK 2239: Övriga specialistsjuksköterskor. 
SSYK 2222: Barnmorskor. 
SSYK 8199: Övriga drifttekniker och processövervakare. 
SSYK 5341: Skötare. 
SSYK 1360: Driftchefer inom bygg, anläggning och gruva. 
SSYK 3153: Piloter m.fl.. 
SSYK 5342: Vårdare, boendestödjare. 
SSYK 8113: Brunnsborrare m.fl.. 
SSYK 4226: Marknadsundersökare och intervjuare. 
SSYK 3323: Inköpare och upphandlare. 
SSYK 3324: Ordersamordnare m.fl.. 
SSYK 2172: Grafiska formgivare m.fl.. 
SSYK 1340: Chefer inom arkitekt- och ingenjörsverksamhet. 
SSYK 8214: Montörer, träprodukter. 
SSYK 3240: Djursjukskötare m.fl.. 
SSYK 4323: Transportledare och transportsamordnare. 
SSYK 9622: Vaktmästare m.fl.. 
SSYK 1610: Chefer inom bank, finans och försäkring. 
SSYK 1380: Förvaltare inom skogsbruk och lantbruk m.fl.. 
SSYK 3321: Försäkringssäljare och försäkringsrådgivare. 
SSYK 7111: Träarbetare, snickare m.fl.. 
SSYK 5113: Guider och reseledare. 
SSYK 7119: Övriga byggnads- och anläggningsarbetare. 
SSYK 5224: Optikerassistenter. 
SSYK 8174: Maskinoperatörer inom ytbehandling, trä. 
SSYK 2652: Musiker, sångare och kompositörer. 
SSYK 3331: Speditörer och transportmäklare. 
SSYK 3421: Professionella idrottsutövare. 
SSYK 2516: IT-säkerhetsspecialister. 
SSYK 5120: Kockar och kallskänkor. 
SSYK 3411: Behandlingsassistenter och socialpedagoger m.fl.. 
SSYK 2414: Traders och fondförvaltare. 
SSYK 4225: Kontorsreceptionister. 
SSYK 2233: Skolsköterskor. 
SSYK 2342: Fritidspedagoger. 
SSYK 3432: Inredare, dekoratörer och scenografer m.fl.. 
SSYK 8350: Matroser och jungmän m.fl.. 
SSYK 3521: Bild- och sändningstekniker. 
SSYK 8191: Drifttekniker vid värme- och vattenverk. 
SSYK 2514: Systemtestare och testledare. 
SSYK 2512: Mjukvaru- och systemutvecklare m.fl.. 
SSYK 7115: Anläggningsdykare. 
SSYK 5169: Övrig servicepersonal. 
SSYK 2614: Affärs- och företagsjurister. 
SSYK 2171: Industridesigner. 
SSYK 9320: Handpaketerare och andra fabriksarbetare. 
SSYK 8193: Processövervakare, metallproduktion. 
SSYK 3422: Idrottstränare och instruktörer m.fl.. 
SSYK 8141: Maskinoperatörer, gummiindustri. 
SSYK 2162: Landskapsarkitekter. 
SSYK 3230: Terapeuter inom alternativmedicin. 
SSYK 3351: Tull- och kustbevakningstjänstemän. 
SSYK 3312: Banktjänstemän. 
SSYK 3334: Fastighetsmäklare. 
SSYK 8331: Buss- och spårvagnsförare. 
SSYK 9120: Bilrekonditionerare, fönsterputsare m.fl.. 
SSYK 5221: Säljande butikschefer och avdelningschefer i butik. 
SSYK 3424: Friskvårdskonsulenter och hälsopedagoger m.fl.. 
SSYK 5151: Städledare och husfruar. 
SSYK 1730: Chefer inom handel. 
SSYK 3515: Webbmaster och webbadministratörer. 
SSYK 3211: Tekniker, bilddiagnostik och medicinteknisk utrustning. 
SSYK 2164: Lantmätare. 
SSYK 5349: Övrig vård- och omsorgspersonal. 
SSYK 2311: Professorer. 
SSYK 3341: Gruppledare för kontorspersonal. 
SSYK 5144: Fotterapeuter. 
SSYK 2260: Tandläkare. 
SSYK 8219: Övriga montörer. 
SSYK 5311: Barnskötare. 
SSYK 7124: Glastekniker. 
SSYK 7211: Gjutare. 
SSYK 4212: Inkasserare och pantlånare m.fl.. 
SSYK 2615: Förvaltnings- och organisationsjurister. 
SSYK 5225: Bensinstationspersonal. 
SSYK 3114: Ingenjörer och tekniker inom maskinteknik. 
SSYK 2111: Fysiker och astronomer. 
SSYK 6121: Uppfödare och skötare av lantbrukets husdjur. 
SSYK 0110: Officerare. 
SSYK 7233: Underhållsmekaniker och maskinreparatörer. 
SSYK 8341: Förare av jordbruks- och skogsmaskiner. 
SSYK 8213: Montörer, metall-, gummi- och plastprodukter. 
SSYK 7214: Tunnplåtslagare. 
SSYK 4112: Löne- och personaladministratörer. 
SSYK 8112: Processoperatörer, stenkross- och malmförädling. 
SSYK 5323: Undersköterskor, vård- och specialavdelning och mottagning. 
SSYK 2112: Meteorologer. 
SSYK 7321: Prepresstekniker. 
SSYK 6222: Fiskare. 
SSYK 6129: Övriga djuruppfödare och djurskötare. 
SSYK 5141: Frisörer. 
SSYK 7231: Motorfordonsmekaniker och fordonsreparatörer. 
SSYK 5226: Uthyrare. 
SSYK 4113: Backofficepersonal m.fl.. 
SSYK 7222: Verktygsmakare. 
SSYK 6122: Uppfödare och skötare av sällskapsdjur. 
SSYK 7413: Distributionselektriker. 
SSYK 3513: Systemadministratörer. 
SSYK 2173: Designer inom spel och digitala medier. 
SSYK 8344: Truckförare. 
SSYK 5241: Eventsäljare och butiksdemonstratörer m.fl.. 
SSYK 2232: Barnsjuksköterskor. 
SSYK 2314: Doktorander. 
SSYK 1790: Övriga chefer inom övrig servicenäring. 
SSYK 7112: Murare m.fl.. 
SSYK 1520: Chefer inom socialt och kurativt arbete. 
SSYK 8329: Övriga bil-, motorcykel- och cykelförare. 
SSYK 2313: Forskarassistenter m.fl.. 
SSYK 2179: Övriga designer och formgivare. 
SSYK 3433: Inspicienter och scriptor m.fl.. 
SSYK 2226: Ambulanssjuksköterskor m.fl.. 
SSYK 2643: Översättare, tolkar och lingvister m.fl.. 
SSYK 2144: Civilingenjörsyrken inom maskinteknik. 
SSYK 2284: Optiker. 
SSYK 2163: Planeringsarkitekter m.fl.. 
SSYK 2282: Dietister. 
SSYK 5242: Telefonförsäljare m.fl.. 
SSYK 1250: Försäljnings- och marknadschefer. 
SSYK 2513: Utvecklare inom spel och digitala media. 
SSYK 8192: Processövervakare, kemisk industri. 
SSYK 1330: Forsknings- och utvecklingschefer. 
SSYK 7131: Målare. 
SSYK 2621: Museiintendenter m.fl.. 
SSYK 2421: Lednings- och organisationsutvecklare. 
SSYK 7611: Slaktare och styckare m.fl.. 
SSYK 5161: Begravnings- och krematoriepersonal. 
SSYK 2319: Övriga universitets- och högskolelärare. 
SSYK 6130: Växtodlare och djuruppfödare, blandad drift. 
SSYK 3151: Maskinbefäl. 
SSYK 3314: Skadereglerare och värderare. 
SSYK 2143: Civilingenjörsyrken inom elektroteknik. 
SSYK 7133: Skorstensfejare. 
SSYK 9331: Hamnarbetare. 
SSYK 8212: Montörer, elektrisk och elektronisk utrustning. 
SSYK 2642: Journalister m.fl.. 
SSYK 2281: Apotekare. 
SSYK 2273: Arbetsterapeuter. 
SSYK 4410: Arkiv- och biblioteksassistenter m.fl.. 
SSYK 7215: Stålkonstruktionsmontörer och grovplåtsslagare. 
SSYK 3335: Fastighetsförvaltare. 
SSYK 8311: Lokförare. 
SSYK 2242: Psykoterapeuter. 
SSYK 2511: Systemanalytiker och IT-arkitekter m.fl.. 
SSYK 7532: Sömmare. 
SSYK 8129: Övriga maskin- och processoperatörer vid stål- och metallverk. 
SSYK 8173: Operatörer inom sågverk, hyvleri och plywood m.m.. 
SSYK 5131: Hovmästare och servitörer. 
SSYK 2412: Controller. 
SSYK 2181: Arbetsmiljöingenjörer, yrkes- och miljöhygieniker. 
SSYK 2519: Övriga IT-specialister. 
SSYK 7122: Golvläggare. 
SSYK 2114: Geologer och geofysiker m.fl.. 
SSYK 3514: Nätverks- och systemtekniker m.fl.. 
SSYK 7523: Maskinsnickare och maskinoperatörer, träindustri. 
SSYK 2241: Psykologer. 
SSYK 8132: Maskinoperatörer, kemisktekniska och fotografiska produkter. 
SSYK 2250: Veterinärer. 
SSYK 5230: Kassapersonal m.fl.. 
SSYK 2431: Marknadsanalytiker och marknadsförare m.fl.. 
SSYK 2415: Nationalekonomer och makroanalytiker m.fl.. 
SSYK 4321: Arbetsledare inom lager och terminal. 
SSYK 1230: Förvaltnings- och planeringschefer. 
SSYK 9610: Renhållnings- och återvinningsarbetare. 
SSYK 6113: Trädgårdsanläggare m.fl.. 
SSYK 5132: Bartendrar. 
SSYK 3451: Köksmästare och souschefer. 
SSYK 5312: Elevassistenter m.fl.. 
SSYK 8343: Kranförare m.fl.. 
SSYK 2142: Civilingenjörsyrken inom bygg och anläggning. 
SSYK 7322: Tryckare. 
SSYK 3213: Receptarier. 
SSYK 2134: Specialister och rådgivare inom lantbruk m.m.. 
SSYK 7116: Ställningsbyggare. 
SSYK 2419: Övriga ekonomer. 
SSYK 4211: Croupierer och oddssättare m.fl.. 
SSYK 5223: Butikssäljare, fackhandel. 
SSYK 8121: Maskinoperatörer, ytbehandling. 
SSYK 2131: Cell- och molekylärbiologer m.fl.. 
SSYK 9111: Städare. 
SSYK 2221: Grundutbildade sjuksköterskor. 
SSYK 2121: Matematiker och aktuarier. 
SSYK 4116: Skolassistenter m.fl.. 
SSYK 3353: Socialförsäkringshandläggare. 
SSYK 7412: Industrielektriker. 
SSYK 9411: Pizzabagare m.fl.. 
SSYK 3423: Fritidsledare m.fl.. 
SSYK 7319: Musikinstrumentmakare och övriga konsthantverkare. 
SSYK 3311: Mäklare inom finans. 
SSYK 8181: Maskinoperatörer, påfyllning, packning och märkning. 
SSYK 2271: Kiropraktorer och naprapater m.fl.. 
SSYK 1113: Chefstjänstemän i intresseorganisationer. 
SSYK 5419: Övrig bevaknings- och säkerhetspersonal. 
SSYK 2225: Psykiatrisjuksköterskor. 
SSYK 3215: Laboratorieingenjörer. 
SSYK 7213: Byggnads- och ventilationsplåtslagare. 
SSYK 4224: Hotellreceptionister m.fl.. 
SSYK 7114: Anläggningsarbetare. 
SSYK 2423: Personal- och HR-specialister. 
SSYK 2619: Övriga jurister. 
SSYK 9332: Ramppersonal, flyttkarlar och varupåfyllare m.fl.. 
SSYK 6221: Fiskodlare. 
SSYK 2515: Systemförvaltare m.fl.. 
SSYK 7123: Isoleringsmontörer. 
SSYK 2411: Revisorer m.fl.. 
SSYK 2312: Universitets- och högskolelektorer. 
SSYK 1530: Chefer inom äldreomsorg. 
SSYK 2231: Operationssjuksköterskor. 
SSYK 7613: Provsmakare och kvalitetsbedömare. 
SSYK 2662: Kuratorer. 
SSYK 7132: Lackerare och industrimålare. 
SSYK 3122: Arbetsledare inom tillverkning. 
SSYK 3339: Övriga förmedlare. 
SSYK 1112: General-, landstings- och kommundirektörer m.fl.. 
SSYK 5152: Fastighetsskötare. 
SSYK 0210: Specialistofficerare. 
SSYK 2133: Farmakologer och biomedicinare. 
SSYK 2228: Intensivvårdssjuksköterskor. 
SSYK 7311: Finmekaniker. 
SSYK 2132: Växt- och djurbiologer. 
SSYK 4119: Övriga kontorsassistenter och sekreterare. 
SSYK 3155: Flygtekniker. 
SSYK 1310: IT-chefer. 
SSYK 1710: Hotell- och konferenschefer. 
SSYK 2213: AT-läkare. 
SSYK 2113: Kemister. 
SSYK 5149: Övriga skönhets- och kroppsterapeuter. 
SSYK 8111: Gruv- och stenbrottsarbetare. 
SSYK 7125: VVS-montörer m.fl.. 
SSYK 7420: Elektronikreparatörer och kommunikationselektriker m.fl.. 
SSYK 4221: Resesäljare och trafikassistenter m.fl.. 
SSYK 8321: Taxiförare m.fl.. 
SSYK 3333: Arbetsförmedlare. 
SSYK 1490: Övriga chefer inom utbildning. 
SSYK 8161: Maskinoperatörer, kött- och fiskberedningsindustri. 
SSYK 2219: Övriga läkare. 
SSYK 4420: Brevbärare och postterminalarbetare. 
SSYK 2653: Koreografer och dansare. 
SSYK 3452: Storhushållsföreståndare. 
SSYK 1720: Restaurang- och kökschefer. 
SSYK 5412: Kriminalvårdare. 
SSYK 3512: Supporttekniker, IT. 
SSYK 8189: Övriga process- och maskinoperatörer. 
SSYK 2655: Skådespelare. 
SSYK 7232: Flygmekaniker m.fl.. 
SSYK 9310: Grovarbetare inom bygg och anläggning. 
SSYK 4115: Inköps- och orderassistenter. 
SSYK 8143: Maskinoperatörer, pappersvaruindustri. 
SSYK 8169: Övriga maskinoperatörer, livsmedelsindustri m.m.. 
SSYK 7134: Saneringsarbetare m.fl.. 
SSYK 9629: Övriga servicearbetare. 
SSYK 4111: Ekonomiassistenter m.fl.. 
SSYK 5413: Väktare och ordningsvakter. 
SSYK 2352: Studie- och yrkesvägledare. 
SSYK 0310: Soldater m.fl.. 
SSYK 5321: Undersköterskor, hemtjänst, hemsjukvård, äldreboende och habilitering. 
SSYK 2122: Statistiker. 
SSYK 2432: Informatörer, kommunikatörer och PR-specialister. 
SSYK 6111: Odlare av jordbruksväxter, frukt och bär. 
SSYK 3152: Fartygsbefäl m.fl.. 
SSYK 8163: Maskinoperatörer, kvarn-, bageri- och konfektyrindustri. 
SSYK 5325: Barnsköterskor. 
SSYK 4322: Lager- och terminalpersonal. 
SSYK 5414: SOS-operatörer m.fl.. 
SSYK 3117: GIS- och kartingenjörer. 
SSYK 8122: Valsverksoperatörer. 
SSYK 1220: Personal- och HR-chefer. 
SSYK 7323: Bokbindare m.fl.. 
SSYK 1210: Ekonomi- och finanschefer. 
SSYK 2359: Övriga pedagoger med teoretisk specialistkompetens. 
SSYK 3119: Övriga ingenjörer och tekniker. 
SSYK 2149: Övriga civilingenjörsyrken. 
SSYK 1240: Informations-, kommunikations- och PR-chefer. 
SSYK 7531: Skräddare och ateljésömmerskor m.fl.. 
SSYK 2669: Övriga yrken inom socialt arbete. 
SSYK 3113: Ingenjörer och tekniker inom elektroteknik. 
SSYK 1320: Inköps-, logistik- och transportchefer. 
SSYK 8312: Bangårdspersonal. 
SSYK 2223: Anestesisjuksköterskor. 
SSYK 2211: Specialistläkare. 
SSYK 3354: Säkerhetsinspektörer m.fl.. 
SSYK 7312: Guld- och silversmeder. 
SSYK 9119: Övrig hemservicepersonal m.fl.. 
SSYK 8114: Maskinoperatörer, cement-, sten- och betongvaror. 
SSYK 3359: Övriga handläggare. 
SSYK 2341: Grundskollärare. 
SSYK 3322: Företagssäljare. 
SSYK 2224: Distriktssköterskor. 
SSYK 4114: Marknads- och försäljningsassistenter. 
SSYK 2413: Finansanalytiker och investeringsrådgivare m.fl.. 
SSYK 1370: Produktionschefer inom tillverkning. 
SSYK 2661: Socialsekreterare. 
SSYK 2182: Miljö- och hälsoskyddsinspektörer. 
SSYK 4222: Kundtjänstpersonal. 
SSYK 2283: Audionomer och logopeder. 
SSYK 7113: Betongarbetare. 
SSYK 3313: Redovisningsekonomer. 
SSYK 7221: Smeder. 
SSYK 2611: Advokater. 
SSYK 1111: Politiker. 
SSYK 9621: Reklamutdelare och tidningsdistributörer. 
SSYK 2145: Civilingenjörsyrken inom kemi och kemiteknik. 
SSYK 6210: Skogsarbetare. 
SSYK 2612: Domare. 
SSYK 2183: Specialister inom miljöskydd och miljöteknik. 
SSYK 3360: Poliser. 
SSYK 2654: Regissörer och producenter av film, teater m.m.. 
SSYK 2146: Civilingenjörsyrken inom gruvteknik och metallurgi. 
SSYK 8131: Maskinoperatörer, farmaceutiska produkter. 
SSYK 5222: Butikssäljare, dagligvaror. 
SSYK 5112: Tågvärdar och ombordansvariga m.fl.. 
SSYK 2272: Fysioterapeuter och sjukgymnaster. 
SSYK 1590: Övriga chefer inom samhällsservice. 
SSYK 8115: Bergsprängare. 
SSYK 2623: Arkeologer och specialister inom humaniora m.m.. 
SSYK 2212: ST-läkare. 
SSYK 8211: Fordonsmontörer. 
SSYK 8159: Övriga maskinoperatörer, textil-, skinn- och läderindustri. 
SSYK 5343: Personliga assistenter. 
SSYK 5330: Vårdbiträden. 
SSYK 9412: Restaurang- och köksbiträden m.fl.. 
SSYK 3112: Ingenjörer och tekniker inom bygg och anläggning. 
SSYK 5411: Brandmän. 
SSYK 5143: Massörer och massageterapeuter. 
SSYK 1290: Övriga administrations- och servicechefer. 
SSYK 7521: Manuella ytbehandlare, trä. 
SSYK 3115: Ingenjörer och tekniker inom kemi och kemiteknik. 
SSYK 3343: Chefssekreterare och VD-assistenter m.fl.. 
SSYK 8116: Stenhuggare m.fl.. 
SSYK 7534: Läderhantverkare och skomakare. 
SSYK 2422: Planerare och utredare m.fl.. 
SSYK 2289: Övriga specialister inom hälso- och sjukvård. 
SSYK 7223: Maskinställare och maskinoperatörer, metallarbete. 
SSYK 7533: Tapetserare. 
SSYK 3121: Arbetsledare inom bygg, anläggning och gruva. 
SSYK 3355: Brandingenjörer och byggnadsinspektörer m.fl.. 
SSYK 3212: Biomedicinska analytiker m.fl.. 
SSYK 7212: Svetsare och gasskärare. 
SSYK 2622: Bibliotekarier och arkivarier. 
SSYK 1740: Chefer inom friskvård, sport och fritid. 
SSYK 1510: Chefer inom hälso- och sjukvård. 
SSYK 2672: Diakoner. 
SSYK 3412: Pastorer m.fl.. 
SSYK 1540: Chefer och ledare inom trossamfund. 
SSYK 2141: Civilingenjörsyrken inom logistik och produktionsplanering. 
SSYK 5142: Hudterapeuter. 
SSYK 3439: Övriga yrken inom kultur och underhållning. 
SSYK 9520: Torg- och marknadsförsäljare m.fl.. 
SSYK 8162: Maskinoperatörer, mejeri. 
SSYK 8142: Maskinoperatörer, plastindustri. 
SSYK 3441: Trafiklärare. 
SSYK 3154: Flygledare. 
SSYK 7411: Installations- och serviceelektriker. 
SSYK 1410: Chefer inom grund- och gymnasieskola samt vuxenutbildning. 
SSYK 3342: Domstols- och juristsekreterare m.fl.. 
SSYK 2641: Författare m.fl.. 
